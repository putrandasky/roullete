export const chips = [
  // { src: require("../../assets/images/chips/0,01_chip.png"), alt: "0,01", price: 0.01, },
  { src: require("../../assets/images/chips/0,1_chip.png"), alt: "0,1", price: 0.1 },
  { src: require("../../assets/images/chips/0,5_chip.png"), alt: "0,5", price: 0.5 },
  { src: require("../../assets/images/chips/1_chip.png"), alt: "1", price: 1 },
  { src: require("../../assets/images/chips/5_chip.png"), alt: "5", price: 5 },
  { src: require("../../assets/images/chips/10_chip.png"), alt: "10", price: 10 },
  { src: require("../../assets/images/chips/25_chip.png"), alt: "25", price: 25 },
  { src: require("../../assets/images/chips/50_chip.png"), alt: "50", price: 50 },
  { src: require("../../assets/images/chips/100_chip.png"), alt: "100", price: 100 }
];
