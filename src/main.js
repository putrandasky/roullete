import Vue from 'vue';
import App from './App.vue';
import store from './store';
import AlertAlert from 'vue-alert-alert';
import UUID from 'vue-uuid';
import './assets/styles/app.scss';

Vue.use(AlertAlert);
Vue.use(UUID);

Vue.config.productionTip = false;
Vue.config.devtools = false;

const promise = new Promise((res) => {
  let instance;

  window.INACTIVITY_TIME = 100000000;
  window.ENABLE_API_LOG = true;
  window.DIRECT_AUTH = true;
  window.LANGUAGE = 'IT';

  window.OnServerAnsReceived = function (oGameData) {
    console.log('OnServerAnsReceived: ', oGameData);
  }

  window.OnServerAuthSuccess = function (oInitData) {
    console.log('OnServerAuthSuccess: ', oInitData);
    res({ instance, oInitData });
  }

  window.OnServerConnClosed = function () {
    // TODO: add on OnServerConnClosed log
  }

  window.OnServerErrHandler = function (msgErr) {
    console.log('OnServerErrHandler: ', msgErr)
  }

  const options = {
    ipAddr: 'gst.gamingroom.biz',
    port: 1331,
    protocol: 'wss',
    idGame: 8,
    user: 'msp',
    token: 1234
  }

  instance = new window.CServerApiMgr(...Object.values(options));
})

Vue.prototype.$tableMap = [
  0, 10, 6, 2, 21, 17, 13, 31, 27, 23, 41, 37, 33, //En Plein 0-12
  8, 4, 1, 9, 5, 19, 15, 12, 20, 16, 29, 25, 22, 30, 26, 39, 35, 32, 40, 36, //A cavallo 
  -1, -1, -1, -1, //Terzina
  18, 14, 28, 24, 38, 34, //Carré
  7, 3, //Tre
  -1, //Quattro
  51, 50, 49, //Colonna
  42, 43, 44, //Mezza Dozzina
  46, 47, 45, 48, //Rosso/Nero/Pari/Dispari
]
promise.then((result) => {
  Vue.prototype.$apiInstance = result.instance
  Vue.prototype.$oInitData = result.oInitData
  new Vue({
    store,
    render: h => h(App)
  }).$mount('#app')
})