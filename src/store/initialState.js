const state = {
  oGameData: null,
  num_count: 13, // Roulette number count
  money: 3500, // Startup money
  moneyStaked: 0, // Total of money staked
  number: 0,
  you_win: 0,
  winsArr: [], // Wins array
  winTot: 0, // Win total
  autoCount: 0,

  max_win: 10000, // Max win
  historyArr: new Array(12), // History
  tableArr: new Array(56), //table array
  placedChips: [], // placed chips data [{place, price}]
  finished_num: 0, // Finished number
  bg_audio_disabled: false, // Mute background audio
  effect_audio_disabled: false, // Mute effect audio
  bg_audio_volume: .5, // intial volume
  effect_audio_volume: .5, // intial volume

  audio_bg: new Audio(require("../assets/sounds/music-bitsnbites-liver.mp3")),
  kickSound1: new Audio(require("../assets/sounds/CasinoChipsPE.mp3")),
  kickSound2: new Audio(require("../assets/sounds/CasinoChipsSE.mp3")),
  kickSound3: new Audio(require("../assets/sounds/WheelSpin.mp3")),

  is_turning: false, // Turning flag
  turning_duration: 2, // Turning Duration
  delay: 1, // turning Delay

  mode: "normalSpin", // normalSpin or autoStart
  turning_speed: 300, // Turning Speed
  turning_deg: 0, // Turning Deg
  wheel_numbers: [9, 2, 7, 6, 12, 4, 0, 5, 11, 3, 10, 1, 8],
  wheel_colors: [ "#D60015", "#000000", "#D60015", "#000000", "#D60015", "#000000", "#19ED14", "#D60015", "#000000", "#D60015", "#000000", "#D60015", "#000000" ],
  latest_result: -1, // Latest Result
  current_result: -1, // Current Result
  drag: false, // Is Dragging
  dragPrice: 0, // Dragging Chip Price

  number_array: [
    [[1, 3], [2, 3]],
    [[7, 2], [8, 2]],
    [[5, 2], [6, 2]],
    [[7, 4], [8, 4]],
    [[3, 1], [4, 1], [1, 4], [2, 4]],
    [[1, 2], [2, 2]],
    [[3, 3], [4, 3]],
    [[5, 1], [6, 1], [3, 4], [4, 4]],
    [[7, 1], [8, 1], [5, 4], [6, 4]],
    [[5, 3], [6, 3]],
    [[7, 3], [8, 3]],
    [[1, 1], [2, 1]],
    [[3, 2], [4, 2]]
  ],
  //By Yulia
  sync_offset: 0, // default offset on number array to get correct result when stopping spinning
  random_rotation_index: 1, // random scss selection index with "_" 1-4
  random_diamond: 1, //random diamond selection index. 1-8
  random_rotation_range: 4, // max range value for wheel random rotation (we can chane it from 1 to 4)
  ball_duration: 1000,
  lackofMoneyForBetting: false,
  disableRepeatbutton: false,
  winSum: 0,
  last_win: 0,
  last_win_flag: false,
  pipe_running:false,
  maxwinSum:0,

  stopAutoPlay$: null, // rxjs subject which stops auto play
  deviceWidth: 0, // height of the device
  deviceHeight: 0, // width of the device
  loading: false // check if all images are loaded
};

state.kickSound1.volume = 0;
state.kickSound2.volume = 0;
state.kickSound3.volume = 0;

export default state;
