import state from './initialState';
import _ from 'lodash';
import Vue from 'vue';

export const getRandomInt = (min, max) => {
  state.random_rotation_index = Math.floor(Math.random()) + 1;
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const getRandomValueFromArray = (items) => {
  return items[Math.floor(Math.random() * items.length)];
}

export const getRouletteResult = (deg, array) => {
  let offset = ((deg % 360) / (360 / state.num_count));
  offset = state.num_count - Math.round(offset);
  let arrayHead = array.slice(0, offset);
  let arrayTail = array.slice(offset);
  array = arrayTail.concat(arrayHead);
  let number_index = 9 - state.syn_offset;

  return array[number_index];
}

export const sendRequestToServer = () => {
  let bids = JSON.parse(localStorage.getItem('placedChips'));
  let tableArr = _.fill(Array(56), 0);

  // console.log("instance: ", tableArr);
  // console.log("placed chips: ", bids);

  let helper = {};
  let stake = 0;
  let placedChips = bids.reduce(function (r, o) {
    let key = o.place;
    stake += o.price;
    if (!helper[key]) {
      helper[key] = Object.assign({}, o);
      r.push(helper[key]);
    } else {
      helper[key].price += o.price;
    }

    return r;
  }, []);

  // console.log(placedChips);
  // console.log("stake: ", stake);

  placedChips.forEach(element => {
    let index = Vue.prototype.$tableMap.indexOf(element.place);
    console.log("index: ", index);
    if (index >= 0) {
      tableArr[index] = element.price;
    }
  });

  // console.log("table array: ", tableArr);
  // console.log("helper apiInstance:", Vue.prototype.$apiInstance);

  Vue.prototype.$apiInstance.SendRequest(stake, tableArr);
}
