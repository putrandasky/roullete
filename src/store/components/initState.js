import _ from 'lodash'
export const SET_INITIAL_STATE = (state, money, historyArr, tableArr) => {
  state.money = parseFloat(money);
  state.historyArr = historyArr ? historyArr : _.fill(Array(12), -1);
  state.tableArr = tableArr ? tableArr : _.fill(Array(56), -1);
}

export const setInitialState = (context, { money, historyArr, tableArr }) => {
  context.commit('SET_INITIAL_STATE', money, historyArr, tableArr)
}