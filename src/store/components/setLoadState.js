export const SET_LOAD_STATE = (state, value) => {
  state.loading = value;
}

export const setLoadState = (context, { value }) => {
  context.commit('SET_LOAD_STATE', value)
}
