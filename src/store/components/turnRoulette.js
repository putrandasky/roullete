import _ from 'lodash';
import { timer, Subject } from 'rxjs';
import { take, takeUntil, finalize, tap } from 'rxjs/operators';
import { getRandomValueFromArray, sendRequestToServer } from "../helpers";
import state from "../initialState";

export const TURN_ROULETTE = (state) => {
  state.autoCount--;
  state.is_turning = true;
  state.drag = false;
  state.you_win = 0;
  state.dragPrice = 0;
  const resultNumber = state.oGameData.number;
  const randomParams = getRandomValueFromArray(state.number_array[resultNumber]);
  console.log("randomParams: ", randomParams);
  state.random_rotation_index = randomParams[1];
  state.random_diamond = randomParams[0];
  console.log('random rotation index: ', state.random_rotation_index);
  state.turning_deg =
    state.random_rotation_index * (360 / state.num_count) + 3 * 360;
  state.kickSound3.currentTime = 1;
  state.kickSound3.play();
};

export const turnRoulette = (context) => {
  context.commit("RESET_CURRENT_RESULT");
  context.commit("TURN_ROULETTE");
};

export const SET_MODE = (state, mode) => {
  console.log("mode----->", mode);
  state.mode = mode;
  if (mode === "autoStart-start") {
    state.turning_duration = 10;
    state.delay = 2;
    state.ball_duration = 1000;

  }
  if (mode === "normalSpin-start") {
    state.turning_duration = 10;
    state.delay = 2;
    state.ball_duration = 1000;

  }
  if (mode === "turbo-start") {
    state.turning_duration = 5;
    state.delay = 1;
    state.ball_duration = 65;
  }
  if (mode === "auto-turbo-start") {
    state.turning_duration = 5;
    state.delay = 1;
    state.ball_duration = 65;

  }
  if (mode === "disableRepeatButton") {
    // state.turning_duration = 10;
    // state.delay = 2;
    // state.ball_duration = 1000;
    state.lackofMoneyForBetting = true;

  }
  if (mode === "auto-turbo-end-ready") {
    state.turning_duration = 10;
    state.delay = 2;
    state.ball_duration = 1000;
    // state.is_turning =false;

  }
};

export const setMode = (context, { mode, count }) => {//for autoplay
  context.commit("SET_MODE", mode);
  state.autoCount = count;
  const replayInterval = mode === "autoStart-start" ? 14500 : 1200;//7000
  const endReadyMessage = mode === "autoStart-start" ? "autoStart-end-ready" : "auto-turbo-end-ready";//
  if (state.mode === "autoStart-start" || state.mode === "auto-turbo-start") {
    let stopAutoPlayByError = false;
    const playInterval = timer(0, replayInterval);
    state.stopAutoPlay$ = new Subject();
    const playAutoStart = playInterval.pipe(
      take(count),
      takeUntil(state.stopAutoPlay$),
      tap(() => {
        const lastStake = JSON.parse(localStorage.getItem('placedChips'));
        context.dispatch("setChips", { chips: lastStake });
        const totalStake = _.sumBy(lastStake, (stake) => stake.price);
        if (context.getters.getMoneyLeft >= totalStake) {
          // context.dispatch("turnRoulette");
          sendRequestToServer()
        }
        if ((context.getters.getMoneyLeft) < totalStake) {
          // alert('lack of money');
          stopAutoPlayByError = true;
          context.commit("SET_MODE", endReadyMessage);//
          // state.stopAutoPlay$.next();
          // state.stopAutoPlay$.complete();

          context.dispatch("stopRoulette");
          context.dispatch("stopAutoPlay");
          // context.commit("SET_MODE", 'disableRepeatButton');
        }
      }),
      finalize(() => {
        timer(500).subscribe(() => {
          if (!stopAutoPlayByError)
            context.commit("SET_MODE", "auto-turbo-end");//endReadyMessage
          else {
            // alert('error with lack of money');
            // context.commit("SET_MODE", "auto-turbo-end");
            state.disableRepeatbutton = true;
            state.placedChips = [];
            // state.is_turning = false;
          }

        });
      })
    );
    playAutoStart.subscribe();
  }

  if (state.mode === "normalSpin-start" || state.mode === "turbo-start") {
    const duration = mode === 'normalSpin-start' ? 10000 : 5000;
    const message = mode === 'normalSpin-start' ? 'normalSpin-end' : 'turbo-end';
    const playNormal = timer(0, duration);
    playNormal.pipe(
      take(1),
      tap(() => {
        // context.dispatch("turnRoulette");
        // console.log("sending request...")
        sendRequestToServer()
      }),
      finalize(() => {
        context.commit("SET_MODE", message);
      })
    ).subscribe();
  }
  // if (mode === "turbo-start") {
  //   context.dispatch("turnRoulette");
  //   setTimeout(() => {
  //     context.commit("SET_MODE", "turbo-end");
  //   }, 6000);
  // }
};

export const STOP_ROULETTE = (state) => {
  if (!state.oGameData) return;
  const resultNumber = state.oGameData.number
  state.latest_result = parseInt(resultNumber)
  state.current_result = parseInt(resultNumber)
  state.is_turning = false;
  state.turning_deg = 0;
  state.money = parseFloat(state.oGameData.money);
  state.winTot = state.oGameData.winTot;
  if(state.winTot > 0)
    state.you_win = 1
  else
    state.you_win = 2
  console.log("state.finished_num: ", state.finished_num)
  state.historyArr[state.finished_num] = state.latest_result;
  state.historyArr = [...state.historyArr];
  state.finished_num++;
  state.kickSound3.pause();

};

export const stopRoulette = ({ commit, state }) => {
  // alert('stip');
  commit("STOP_ROULETTE");
  if (state.mode === "autoStart-end-ready") {
    commit("SET_MODE", "autoStart-end");
  }
  if (state.mode === "auto-turbo-end-ready") {
    commit("SET_MODE", "auto-turbo-end");
  }
};

export const RESET_CURRENT_RESULT = (state) => {
  state.current_result = -1
};

export const resetCurrentResult = ({ commit }) => {
  commit("RESET_CURRENT_RESULT");
};

export const STOP_AUTO_PLAY = (state) => {

  state.stopAutoPlay$.next();
  state.stopAutoPlay$.complete();
  // state.disableRepeatbutton = true;


}
export const stopAutoPlay = ({ commit }) => {
  commit("STOP_AUTO_PLAY");
}