--------------------------------------------------------------------------------------------------------------------------------------------------------------
tasks:

[-]  In progress
[+]  Done
[$]  Needs discussion

1. [+] REMOVE HIGHLIGHTED NUMBER ON THE WHEEL
2. [+] additional: clean-up branch, added favicon, optimized assets, cleaned syntax (format, comments, unused classes...), restructured folders
3. [+] Number display area in the middle of the wheel: (Firefox, Chrome and Safari) / add new design based on number color
4. [+] Put AUDIO button between the TOTAL BET and the SETTINGS button following the new design
    a. [+] The inserted AUDIO button must open the two scroll bars following the new XD
    b. [+] The game has to start with a 50% volume set default, now is too high
    c. [+] show mute icon if both sliders are on 0
    d. [+] remove volumes control from settings
5. [$] globally check/change fonts to match the new design
6. [-] add 3 popups on certain events - You Win, You Lose and Game Over - Following the new XD
7. [+] Replace the keys in this order:
  a. [+] 1) Info 2) Scelta lingua 3)  4) CRONOLOGIA 5) ACCOUNT UTENTE 6) AGGIUNGI FONDI 7) ALTRI GIOCHI
  b. [+] add “CASH AND EXIT” button as the same in the ref, following the new XD
  c. [+] find a way to differ the settings based on the domain (see Settings.vue:112)
  d. [+] The language dropdown has to be a bit upper because we can see the corners
8. [+] redesign settings container (thinner border + new layout)
9. [$] Summary (footer) - correct the spacing according to the layout //- is there a mockup or any explanation on how to 'correct?'
10. [$] Moving the roulette wheel: On the Firefox browser at the end of the spin, the entire screen sometimes moves. //- how to reproduce the issue?
11. [$] make the platform responsive and nice looking on all devices and orientation modes //- can you provide details per screen size and orientation, please - otherwise it increases the time needed
  a. [+] The latest numbers are out of the container
  b. [-] position audio modal relative to audio settings button
12. [+] changed 'RED' to 'ROSSO' and 'BLACK' to 'NERO'

--------------------------------------------------------------------------------------------------------------------------------------------------------------
suggestions:
- show warnings in modal instead of alert (see reqs/alert.jpg)
- make main container always fit the screen height on landscape and screen width on portrait
- use css to style all the buttons along side with some icon library
- use css framework (like bootstrap) to redo responsiveness according to the best practices
- conventionally optimize the syntax and document the code
- optimize all images by indexing them
